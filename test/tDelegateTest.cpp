/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018, 2019 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file tests the delegate class.

   2019-02-06, Bin Gao:
   * first version
*/

#include <iostream>

#include "tGlueCore/tDelegate.hpp"

void free_function(int& number) { number = 2019; }

class class_function
{
    public:
        explicit class_function() = default;
        int get_integer() const noexcept { return 2019; }
        ~class_function() noexcept = default;
};

class functor
{
    public:
        explicit functor() noexcept = default;
        int operator() () const noexcept { return 2019; }
        ~functor() noexcept = default;
};

int main()
{
    int return_number;
    bool lambda_type;
    /* TODO: compile error for auto lambda_function = []... */
    auto lambda_function = [=](int& number)->bool { number = 2019; return true; };
    std::shared_ptr<class_function> test_class = std::make_shared<class_function>(class_function());
    std::shared_ptr<functor> test_functor = std::make_shared<functor>(functor());

    tGlueCore::tDelegateState delegate_state;
    tGlueCore::tDelegate<bool(int&)> lambda_delegate;
    tGlueCore::tDelegate<void(int&)> free_delegate;
    tGlueCore::tDelegate<int(void)> class_delegate;
    tGlueCore::tDelegate<int(void)> functor_delegate;

    lambda_delegate.set(lambda_function);
    free_delegate.set(free_function);
    class_delegate.set(test_class, &class_function::get_integer);
    functor_delegate.set(test_functor);

    std::cout << std::boolalpha;

    delegate_state = lambda_delegate.invoke(lambda_type, return_number);
    std::cout << "Test delegate of lambda function -> pass="
              << (delegate_state==tGlueCore::tDelegateState::Callable)
              << ", number=" << return_number << "\n";

    delegate_state = free_delegate.invoke<void>(return_number);
    std::cout << "Test delegate of free method -> pass="
              << (delegate_state==tGlueCore::tDelegateState::Callable)
              << ", number=" << return_number << "\n";

    delegate_state = class_delegate.invoke(return_number);
    std::cout << "Test delegate of class method -> pass="
              << (delegate_state==tGlueCore::tDelegateState::Callable)
              << ", number=" << return_number << "\n";

    delegate_state = functor_delegate.invoke(return_number);
    std::cout << "Test delegate of functor -> pass="
              << (delegate_state==tGlueCore::tDelegateState::Callable)
              << ", number=" << return_number << "\n";

    return 0;
}
