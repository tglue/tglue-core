/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018, 2019 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the logger class of tGlue ecosystem.

   2017-11-18, Bin Gao:
   * moved a few implementations from the header file
*/

#include "tGlueCore/Logger.hpp"

namespace tGlueCore
{
    void Logger::log_stringify() noexcept
    {
        if (empty()) {
            ostream_logger(MessageType::Warning,
                           std::string("No log handler provided. Use insert() to add log handler(s).\n"));
            ostream_logger(m_log_type, m_log_content);
        }
        else {
            /* Invoke log handler(s) to write */
            auto states = pasv_invoke(std::forward<MessageType>(m_log_type), m_log_content);
            bool failed = false;
            std::ostringstream err_stream;
            for (auto istate=states.cbegin(); istate!=states.cend(); ++istate) {
                switch (*istate) {
                    case DelegateState::Callable:
                        break;
                    case DelegateState::Empty:
                        failed = true;
                        err_stream << "Log handler"
                                   << std::distance(states.cbegin(), istate)
                                   << "has no stored object's method.\n";
                        break;
                    case DelegateState::Invoked:
                        failed = true;
                        err_stream << "Log handler"
                                   << std::distance(states.cbegin(), istate)
                                   << "is being invoked.\n";
                        break;
                    default:
                        failed = true;
                        err_stream << "Log handler"
                                   << std::distance(states.cbegin(), istate)
                                   << "has error when invoked.\n";
                        break;
                }
            }
            if (failed) {
                err_stream << "Erroneous log handler(s) detected. Use remove(delegate_id) to remove them.\n";
                ostream_logger(MessageType::Error, err_stream.str());
            }
        }
        m_log_content.clear();
    }

    void Logger::ostream_logger(const MessageType logType,
                                const std::string& logContent) noexcept
    {
        /* Prepares prompt */
        std::string str_prompt;
        switch (logType) {
            case MessageType::Input:
                str_prompt = "[In] ";
                break;
            case MessageType::Output:
                str_prompt = "[Out] ";
                break;
            case MessageType::Debug:
                str_prompt = "[Debug] ";
                break;
            case MessageType::Warning:
                str_prompt = "[Warning] ";
                break;
            case MessageType::Error:
                str_prompt = "[Error] ";
                break;
        }
        /* Positions of blank found */
        std::size_t next_blank;
        std::size_t prev_blank;
        std::size_t len_prompt = str_prompt.size();
        std::size_t len_content = logContent.size();
        /* Writes the whole string */
        if (len_prompt+len_content<=m_len_line) {
            if (m_colorized) {
            }
            else {
                *m_log_ostream << str_prompt << logContent;
            }
        }
        else {
            /* Writes the first line with the given prompt */
            std::size_t begin_char = 0;
            std::size_t end_char = m_len_line-len_prompt;
            /* Words are separated by space or punctuation marks, we write the
               first line if the last character of this line is a space */
            if (logContent[end_char]==' ') {
                if (m_colorized) {
                }
                else {
                    *m_log_ostream << str_prompt
                                   << logContent.substr(begin_char, end_char);
                }
            }
            /* We search for the nearest space if the last character is not a space */
            else {
                next_blank = logContent.find(' ', end_char);
                if (next_blank==std::string::npos) next_blank = len_content;
                prev_blank = logContent.rfind(' ', end_char);
                if (prev_blank==std::string::npos) prev_blank = next_blank;
                if (end_char+end_char<prev_blank+next_blank) {
                    if (m_colorized) {
                    }
                    else {
                        *m_log_ostream << str_prompt
                                       << logContent.substr(begin_char, prev_blank-begin_char);
                    }
                    end_char = prev_blank;
                }
                else {
                    if (m_colorized) {
                    }
                    else {
                        *m_log_ostream << str_prompt
                                       << logContent.substr(begin_char, next_blank-begin_char);
                    }
                    end_char = next_blank;
                }
            }
            /* Writes left lines */
            begin_char = end_char+1;
            end_char = begin_char+m_len_line;
            if (end_char>=len_content) end_char = len_content-1;
            std::string indent_string = std::string(len_prompt, ' ');
            for (; begin_char<len_content; ) {
                if (logContent[end_char]==' ') {
                    *m_log_ostream << "\n"
                                   << indent_string
                                   << logContent.substr(begin_char, end_char-begin_char+1);
                }
                else {
                    next_blank = logContent.find(' ', end_char);
                    if (next_blank==std::string::npos) next_blank = len_content;
                    prev_blank = logContent.rfind(' ', end_char);
                    if (prev_blank==std::string::npos) prev_blank = next_blank;
                    if (end_char+end_char<prev_blank+next_blank) {
                        *m_log_ostream << "\n"
                                       << indent_string
                                       << logContent.substr(begin_char, prev_blank-begin_char);
                        end_char = prev_blank;
                    }
                    else {
                        *m_log_ostream << "\n"
                                       << indent_string
                                       << logContent.substr(begin_char, next_blank-begin_char);
                        end_char = next_blank;
                    }
                }
                begin_char = end_char+1;
                end_char = begin_char+m_len_line;
                if (end_char>=len_content) end_char = len_content-1;
            }
        }
        /* Add a new line at the end */
        *m_log_ostream << "\n";
    }
}
