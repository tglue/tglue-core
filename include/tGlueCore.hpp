/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This is the header file of tGlueCore library.

   2019-04-04, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/Convert.hpp"
#include "tGlueCore/Stringify.hpp"
#include "tGlueCore/Delegate.hpp"
#include "tGlueCore/Observer.hpp"
#include "tGlueCore/SingletonHolder.hpp"
#include "tGlueCore/Logger.hpp"
