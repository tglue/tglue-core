/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines the singleton implementation used in tGlue system.

   2017-10-04, Bin Gao:
   * first version
*/

#pragma once

#include <mutex>
#include <new>

namespace tGlueCore
{
    /* The implementation of the singleton in tGlue is motivated by the great
       book: "Modern C++ Design: Generic Programming and Design Patterns
       Applied", Andrei Alexandrescu, 2001, Addison-Wesley

       But we choose a much simpler manner without many policies and without
       lifetime control, and we also use std::lock_guard for the multithreaded
       problem; In case of lifetime control, we can introduce the concept of
       "longevity" as did in the above book */
    template<typename T> class SingletonHolder final
    {
        public:
            /* Returns pointer to the singleton object of the class or null
               pointer if allocation of memory failed */
            static T* instance() noexcept
            {
                static SingletonCleanup m_cleanup;
                std::lock_guard<std::mutex> guard(m_mutex);
                if (m_instance==nullptr) {
                    m_instance = new(std::nothrow) T;
                }
                return m_instance;
            }
        protected:
            /* Embedded class to make sure the singleton instance gets deleted on
               program shutdown, modified from
               http://www.cplusplus.com/forum/unices/132540 */
            friend class SingletonCleanup;
            class SingletonCleanup final
            {
                public:
                    ~SingletonCleanup() noexcept
                    {
                        std::lock_guard<std::mutex> guard(SingletonHolder<T>::m_mutex);
                        delete SingletonHolder<T>::m_instance;
                        SingletonHolder<T>::m_instance = nullptr;
                    }
            };
        private:
            /* Pointer to the class */
            static T* m_instance;
            /* For thread safety */
            static std::mutex m_mutex;
            /* Protection */
            explicit SingletonHolder() noexcept;
            ~SingletonHolder() noexcept;
            SingletonHolder(const SingletonHolder&) noexcept;
            SingletonHolder& operator=(const SingletonHolder&) noexcept;
    };
    /* Singleton data initialization and definition */
    template<typename T> T* SingletonHolder<T>::m_instance = nullptr;
    template<typename T> typename std::mutex SingletonHolder<T>::m_mutex;
}
