/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018, 2019 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file converts C++ objects to a string.

   2019-06-06, Bin Gao:
   * first version
*/

#pragma once

#include <iomanip>
#include <ios>
#include <iterator>
#include <sstream>
#include <string>
#include <type_traits>

namespace tGlueCore
{
    /* Convert a list of integral type numbers to a string */
    template<typename T>
    inline typename std::enable_if<std::is_integral<typename std::iterator_traits<T>::value_type>::value, std::string>::type
    stringify(T beginNumber, T endNumber) noexcept
    {
        std::stringstream str_numbers;
        T number = beginNumber;
        str_numbers << '[' << *number;
        ++number;
        for (; number!=endNumber; ++number) {
            str_numbers << ", " << *number;
        }
        str_numbers << ']';
        return str_numbers.str();
    }

    /* Convert a list of floating type numbers to a string */
    template<typename T>
    inline typename std::enable_if<std::is_floating_point<typename std::iterator_traits<T>::value_type>::value, std::string>::type
    stringify(T beginNumber,
              T endNumber,
              std::ios_base &(*notation)(std::ios_base &)=std::scientific,
              const unsigned int decimal=6) noexcept
    {
        std::stringstream str_numbers;
        T number = beginNumber;
        str_numbers << notation << std::setprecision(decimal) << '[' << *number;
        ++number;
        for (; number!=endNumber; ++number) {
            str_numbers << ", " << *number;
        }
        str_numbers << ']';
        return str_numbers.str();
    }

    /* Convert a list of objects to a string, where each object should define a to_string() function */
    template<typename T>
    inline typename std::enable_if<std::is_class<typename std::iterator_traits<T>::value_type>::value, std::string>::type
    stringify(T beginObject, T endObject) noexcept
    {
        std::stringstream str_objects;
        T object = beginObject;
        str_objects << '[' << object->to_string();
        ++object;
        for (; object!=endObject; ++object) {
            str_objects << ", " << object->to_string();
        }
        str_objects << ']';
        return str_objects.str();
    }
}
