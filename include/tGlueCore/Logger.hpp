/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines logger class of tGlue system.

   2017-10-05, Bin Gao:
   * change to use singleton template in tGlueCore/tSingleton.hpp

   2017-09-25, Bin Gao:
   * change to singleton

   2017-05-31, Bin Gao:
   * first version
*/

#pragma once

#include <iostream>
#include <iterator>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>

#include "tGlueCore/Observer.hpp"
#include "tGlueCore/SingletonHolder.hpp"

namespace tGlueCore
{
    /* tGlue message types */
    enum class MessageType {
        Input,
        Output,
        Debug,
        Warning,
        Error
    };

    /* Logger class */
    class Logger final: public Observer<void(const MessageType,const std::string&)>
    {
        public:
            explicit Logger() noexcept:
                m_log_type(MessageType::Output),
                m_log_ostream(&std::cerr),
                m_len_line(90),
                m_colorized(false) {}
            /* Disable copy-construct and copy-assign methods */
            Logger(const Logger& other) noexcept = delete;
            Logger& operator=(const Logger&) noexcept = delete;
            ~Logger() noexcept = default;
            /* Sets the default output stream log handler */
            inline void set_ostream_logger(std::ostream* logStream=&std::cerr,
                                           const unsigned int lenLine=90,
                                           const bool colorized=false) noexcept
            {
                m_log_ostream = logStream;
                m_len_line = lenLine;
                m_colorized = colorized;
            }
            /* Writes log and accepts multiple arguments */
            template<typename... Args>
            inline void write(const MessageType logType, Args const&... args) noexcept
            {
                m_log_type = logType;
                log_stringify(args...);
            }
            /* Default lightweight output stream log handler when is no user
               specified log handler(s) available */
            void ostream_logger(const MessageType logType,
                                const std::string& logContent) noexcept;
        private:
            /* Log type and content */
            MessageType m_log_type;
            std::string m_log_content;
            /* Default lightweight output stream log handler when is no user
               specified log handler(s) available */
            std::ostream* m_log_ostream;
            unsigned int m_len_line;
            bool m_colorized;
            /* Makes pasv_invoke() and actv_invoke() methods private, so
               that users can only use write() member function */
            using Observer<void(const MessageType,const std::string&)>::pasv_invoke;
            /* Called by the write() member function, to write multiple
               arguments into std::string and to call log handler(s) */
            void log_stringify() noexcept;
            template<typename T, typename... Args>
            inline typename std::enable_if<!std::is_enum<T>::value>::type
            log_stringify(T const& logContent, Args const&... args) noexcept
            {
                std::ostringstream log_stream;
                log_stream << logContent;
                m_log_content += log_stream.str();
                log_stringify(args...);
            }
            /* For enum type */
            template<typename T, typename... Args>
            inline typename std::enable_if<std::is_enum<T>::value>::type
            log_stringify(T const& logContent, Args const&... args) noexcept
            {
                std::ostringstream log_stream;
                log_stream << static_cast<typename std::underlying_type<T>::type>(logContent);
                m_log_content += log_stream.str();
                log_stringify(args...);
            }
    };

    /* Logger class singleton */
    using LoggerSingleton = SingletonHolder<Logger>;
}
