/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file declares the delegate for an object's method.

   2019-01-31, Bin Gao:
   * use std::shared_ptr for class and functor objects

   2019-01-30, Bin Gao:
   * remove the use of static_cast() and the storage of an object pointer
   * remove printing information of the object's method, which is not useful

   2017-10-24, Bin Gao:
   * first version
*/

#pragma once

#include <functional>
#include <memory>
#include <type_traits>

namespace tGlueCore
{
    /*@

      == Delegate

      This is a lightweight delegate class to store and later invoke:

      . a `std::function` object,
      . a free function,
      . a class member function, or
      . a functor object.

      I (Bin Gao) learnt mostly from the following projects:

      * https://www.codeproject.com/Articles/11015/The-Impossibly-Fast-C-Delegates?dis
      * https://github.com/NoAvailableAlias/nano-signal-slot
      * https://github.com/vdksoft/signals

      But the class `Delegate` is more lightweight, and different from the
      above projects:

      . it is built on top of `std::function` (as a stub function) and does not
        use any C++ cast operator,
      . it requires std::shared_ptr for a class or a functor object so that one
        does not need to worry an object will be offline later.

      When the delegate is invoked, we need to prevent it from being called
      again by the stored object's method. A simple method is to use a bool
      variable to indicate the delegate is being invoked or not, as implemented
      in the class `Delegate`.

      This method can however not prevent the stored object's method from
      calling iteself by creating new delegate when it is invoked. Such a
      situation should be avoided on the side of the stored object's method.

      Another issue is whether we need to consider the class `Delegate` thread
      safe.

      @enum:DelegateState[State of a delegate.]
      @member:Callable[A delegate is callable.]
      @member:Empty[A delegate has no stored object's method.]
      @member:Invoked[A delegate is being invoked.]

      This enum class object is returned when a delegate is invoked, which
      tells whether the delegate worked well. */
    enum class DelegateState
    {
        Callable,
        Empty,
        Invoked
    };

    /* @class:Delegate[A lightweight delegate class for an object's method.]
       @tparam:ReturnType[Return type of the object's method.]
       @tparam:... Args[Argument types of the object's method.] */
    template<typename ReturnType, typename... Args> class Delegate;
    template<typename ReturnType, typename... Args>
    class Delegate<ReturnType(Args...)>
    {
        public:
            /* @fn:Delegate()[Constructor.] */
            explicit Delegate() noexcept: m_invoked(false) {}
            /* @fn:set[Set a `std::function`.]
               @param[in]:stdFun[The `std::function`.] */
            inline void set(const std::function<ReturnType(Args...)> stdFun) noexcept { m_stub = stdFun; }
            /* @fn:set[Set a free function (or a static method).]
               @param[in]:funPtr[The free function.] */
            inline void set(ReturnType (*funPtr)(Args...)) noexcept { m_stub = funPtr; }
            /* @fn:set[Set a class member function.]
               @param[in]:objPtr[A pointer to the class object.]
               @param[in]:funPtr[The class member function.] */
            template<typename ClassType>
            inline void set(std::shared_ptr<ClassType> objPtr,
                            ReturnType(ClassType::*funPtr)(Args...)) noexcept
            {
                m_stub = [objPtr, funPtr](Args... args)->ReturnType {
                    return ((*objPtr).*funPtr)(std::forward<Args>(args)...);
                };
            }
            /* We implement two template functions for

               . the case of a member function, and
               . the case of a const member function. */
            template<typename ClassType>
            inline void set(std::shared_ptr<ClassType> objPtr,
                            ReturnType(ClassType::*funPtr)(Args...) const) noexcept
            {
                m_stub = [objPtr, funPtr](Args... args)->ReturnType {
                    return ((*objPtr).*funPtr)(std::forward<Args>(args)...);
                };
            }
            /* @fn:set[Set a functor object.]
               @param[in]:funcPtr[A pointer to the functor object.] */
            template<typename FunctorType>
            inline void set(std::shared_ptr<FunctorType> funcPtr) noexcept
            {
                m_stub = [funcPtr](Args... args)->ReturnType {
                    return ((*funcPtr).operator())(std::forward<Args>(args)...);
                };
            }
            /* @fn:invoke[Invoke the object's method.]
               @tparam:InvokeType[Return type of the object's method, should be the same as `ReturnType`.]
               @param[out]:result[Result of the object's method.]
               @param[in]:args[Arguments of the object's method.]
               @return:[State of the delegate.]

               The parameter `result` is only required if `ReturnType` is not `void`. */
            template<typename InvokeType>
            inline typename std::enable_if<std::is_void<ReturnType>::value && std::is_void<InvokeType>::value, DelegateState>::type
            invoke(Args&&... args) noexcept
            {
                if (m_stub) {
                    if (m_invoked) {
                        return DelegateState::Invoked;
                    }
                    else {
                        m_invoked = true;
                        m_stub(std::forward<Args>(args)...);
                        m_invoked = false;
                        return DelegateState::Callable;
                    }
                }
                else {
                    return DelegateState::Empty;
                }
            }
            /* When `ReturnType` is not `void`, users need to provide a
               `InvokeType` (`==ReturnType`) argument to the function
               `invoke()`. */
            template<typename InvokeType>
            inline typename std::enable_if<!std::is_void<ReturnType>::value && std::is_same<InvokeType, ReturnType>::value, DelegateState>::type
            invoke(InvokeType& result, Args&&... args) noexcept
            {
                if (m_stub) {
                    if (m_invoked) {
                        return DelegateState::Invoked;
                    }
                    else {
                        m_invoked = true;
                        result = m_stub(std::forward<Args>(args)...);
                        m_invoked = false;
                        return DelegateState::Callable;
                    }
                }
                else {
                    return DelegateState::Empty;
                }
            }
            /* @fn:operator bool[Check if callable.]
               @return:[`true` if the delegate is callable, false otherwise.] */
            inline operator bool() const noexcept { return m_stub ? true : false; }
            /* @fn:~Delegate()[Deconstructor.] */
            virtual ~Delegate() noexcept = default;
        protected:
            /*@@ Stub function to invoke the object's method */
            std::function<ReturnType(Args...)> m_stub;
            /* Indicates if the delegate is invoked or not */
            bool m_invoked;
    };
}
