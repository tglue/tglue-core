/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements function for type convert.

   2020-07-13, Bin Gao:
   * first version
*/

#pragma once

#include <type_traits>

namespace tGlueCore
{
    /* Convert enum class to its underlying integral type */
    template<typename T>
    inline typename std::underlying_type<T>::type to_integral(const T enumerator) noexcept
    {
        return static_cast<typename std::underlying_type<T>::type>(enumerator);
    }
}
