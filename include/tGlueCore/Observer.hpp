/* tGlueCore: core library of tGlue ecosystem

   Copyright 2018-2020 UiT The Arctic University of Norway

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file declares the observer pattern in the tGlue ecosystem.

   2019-01-31, Bin Gao:
   * error handling will be taken care by tGlueJIT, we therefore do not need to
     enforce return type as expected<T,E> class
   * merge tDelegate class

   2017-11-18, Bin Gao:
   * enforce return type as expected<T,E> class because we anyway need to
     figure out if concrete observers execute normally or not, enforing return
     type letts us only consider one type expected<T,E> instead of checking if
     there is any exception or if the results from concrete observers are
     correct

   2017-10-24, Bin Gao:
   * first version
*/

#pragma once

#include <cstdint>
#include <type_traits>
#include <utility>
#include <vector>

#include "tGlueCore/Delegate.hpp"

namespace tGlueCore
{
    /*@

      == Observer pattern

      This is a lightweight observer pattern built on top of the class
      `Delegate`. There are however few issues to be fixed:

      * make the implementation threadsafe;
      * prevent from calling invoke() from one concrete observer

      @class:Observer[Class for the observer pattern.]
      @tparam:ReturnType[Return type of observers' methods.]
      @tparam:... Args[Argument types of observers' methods.] */
    template<typename ReturnType, typename... Args> class Observer;
    template<typename ReturnType, typename... Args>
    class Observer<ReturnType(Args...)>
    {
        public:
            /* @typedef:size_type[Size type of the observer.] */
            typedef typename std::size_t size_type;
            /* @typedef:delegate_id[Delegate ID of an inserted concrete observer.] */
            typedef typename std::pair<uintptr_t,size_type> delegate_id;
            /* @fn:Observer()[Constructor.] */
            explicit Observer() noexcept: m_counter(0) {}
            /* @fn:insert[Insert a `std::function` as a concrete observer.]
               @param[in]:stdFun[The `std::function`.]
               @return:[Delegate ID of the inserted concrete observer.] */
            inline delegate_id insert(const std::function<ReturnType(Args...)> stdFun) noexcept
            {
                Delegate<ReturnType(Args...)> delegate;
                delegate.set(stdFun);
                m_delegates.push_back(std::make_pair(m_counter, std::move(delegate)));
                return std::make_pair(reinterpret_cast<uintptr_t>(this), m_counter++);
            }
            /* @fn:insert[Insert a free function (or a static method) as a concrete observer.]
               @param[in]:funPtr[The free function.]
               @return:[Delegate ID of the inserted concrete observer.] */
            inline delegate_id insert(ReturnType (*funPtr)(Args...)) noexcept
            {
                Delegate<ReturnType(Args...)> delegate;
                delegate.set(funPtr);
                m_delegates.push_back(std::make_pair(m_counter, std::move(delegate)));
                return std::make_pair(reinterpret_cast<uintptr_t>(this), m_counter++);
            }
            /* @fn:insert[Insert a class member function as a concrete observer.]
               @param[in]:objPtr[A pointer to the class object.]
               @param[in]:funPtr[The class member function.]
               @return:[Delegate ID of the inserted concrete observer.] */
            template<typename ClassType>
            inline delegate_id insert(std::shared_ptr<ClassType> objPtr,
                                      ReturnType(ClassType::*funPtr)(Args...)) noexcept
            {
                Delegate<ReturnType(Args...)> delegate;
                delegate.set(objPtr, funPtr);
                m_delegates.push_back(std::make_pair(m_counter, std::move(delegate)));
                return std::make_pair(reinterpret_cast<uintptr_t>(this), m_counter++);
            }
            /* We implement two template functions for

               . the case of a member function, and
               . the case of a const member function. */
            template<typename ClassType>
            inline delegate_id insert(std::shared_ptr<ClassType> objPtr,
                                      ReturnType(ClassType::*funPtr)(Args...) const) noexcept
            {
                Delegate<ReturnType(Args...)> delegate;
                delegate.set(objPtr, funPtr);
                m_delegates.push_back(std::make_pair(m_counter, std::move(delegate)));
                return std::make_pair(reinterpret_cast<uintptr_t>(this), m_counter++);
            }
            /* @fn:insert[Insert a functor object as a concrete observer.]
               @param[in]:funcPtr[A pointer to the functor object.]
               @return:[Delegate ID of the inserted concrete observer.] */
            template<typename FunctorType>
            inline delegate_id insert(std::shared_ptr<FunctorType> funcPtr) noexcept
            {
                Delegate<ReturnType(Args...)> delegate;
                delegate.set(funcPtr);
                m_delegates.push_back(std::make_pair(m_counter, std::move(delegate)));
                return std::make_pair(reinterpret_cast<uintptr_t>(this), m_counter++);
            }
            /* @fn:pasv_invoke[Passively invoke all concrete observers and ignore any return value.]
               @param[in]:args[Arguments of concrete observers.]
               @return:[States of concrete observers.] */
            template<typename InvokeType = ReturnType>
            inline typename std::enable_if<std::is_void<InvokeType>::value, std::vector<DelegateState>>::type
            pasv_invoke(Args&&... args) noexcept
            {
                std::vector<DelegateState> states;
                for (auto iter=m_delegates.begin(); iter!=m_delegates.end(); ++iter) {
                    states.push_back((iter->second).template invoke<void>(std::forward<Args>(args)...));
                }
                return states;
            }
            template<typename InvokeType = ReturnType>
            inline typename std::enable_if<!std::is_void<InvokeType>::value, std::vector<DelegateState>>::type
            pasv_invoke(Args&&... args) noexcept
            {
                std::vector<DelegateState> states;
                for (auto iter=m_delegates.begin(); iter!=m_delegates.end(); ++iter) {
                    ReturnType result;
                    states.push_back((iter->second).template invoke<ReturnType>(result, std::forward<Args>(args)...));
                }
                return states;
            }
            /* If `ReturnType` is not `void`, user can also use the following
               function `actv_invoke()`.

               @fn:actv_invoke[Invoke all concrete observers but return values are taken care by a user provided operator.]
               @tparam:ActiveOper[Type of the user provided operator.]
               @param[in]:activeOper[The user provided operator.]
               @param[in]:args[Arguments of concrete observers.]
               @return:[Whether invoked all concrete observers successfully.]

               This function is motivated by the `emit_accumulate()` method of
               link:https://github.com/NoAvailableAlias/nano-signal-slo[nano-signal-slot].
               Users can use this function, for instance, to accumulate return
               values from concrete observers. */
            template<typename InvokeType = ReturnType, typename ActiveOper>
            inline typename std::enable_if<!std::is_void<InvokeType>::value, bool>::type
            actv_invoke(ActiveOper&& activeOper, Args&&... args) noexcept
            {
                bool failed = false;
                for (auto iter=m_delegates.begin(); iter!=m_delegates.end(); ++iter) {
                    ReturnType result;
                    auto state = (iter->second).template invoke<ReturnType>(result, std::forward<Args>(args)...);
                    if (state!=DelegateState::Callable) failed = true;
                    activeOper(iter, state, result);
                }
                return failed;
            }
            /* @fn:find[Find a concrete observer according to its delegate ID.]
               @param[in]:delegateId[Delegate ID.]
               @return:[Whether the concrete observer is found or not.] */
            inline bool find(const delegate_id delegateId) const noexcept
            {
                if (delegateId.first==reinterpret_cast<uintptr_t>(this)) {
                    for (auto iter=m_delegates.cbegin(); iter!=m_delegates.cend(); ++iter) {
                        if (delegateId.second==iter->first) return true;
                    }
                }
                return false;
            }
            /* @fn:remove[Remove a concrete observer according to its delegate ID.]
               @param[in]:delegateId[Delegate ID.]
               @return:[true if the concrete observer is found and removed.] */
            inline bool remove(const delegate_id delegateId) noexcept
            {
                if (delegateId.first==reinterpret_cast<uintptr_t>(this)) {
                    for (auto iter=m_delegates.cbegin(); iter!=m_delegates.cend(); ++iter) {
                        if (delegateId.second==iter->first) {
                            m_delegates.erase(iter);
                            return true;
                        }
                    }
                }
                return false;
            }
            /* @fn:size[Return the size (number of concrete observers) of the list of observers.]
               @return:[Size of the list of observers.] */
            inline size_type size() const noexcept { return m_delegates.size(); }
            /* @fn:empty[Test whether the observer list is empty.]
               @return:[true if the observer list is empty.] */
            inline bool empty() const noexcept { return m_delegates.empty(); }
            /* @fn:clear[Remove all concrete observers.] */
            inline void clear() noexcept { m_delegates.clear(); }
            /* @fn:~Observer()[Deconstructor.] */
            virtual ~Observer() noexcept = default;
        private:
            /*@@ Delegates of concrete observers */
            std::vector<std::pair<size_type,Delegate<ReturnType(Args...)>>> m_delegates;
            /* Counter of concrete observers */
            size_type m_counter;
    };
}
